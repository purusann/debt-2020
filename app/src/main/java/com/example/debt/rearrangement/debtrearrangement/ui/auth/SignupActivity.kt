package com.example.debt.rearrangement.debtrearrangement.ui.auth

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns
import android.widget.Toast
import com.example.debt.rearrangement.debtrearrangement.R
import com.example.debt.rearrangement.debtrearrangement.model.Users
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_signin.*
import kotlinx.android.synthetic.main.activity_signup.*

class SignupActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)
        auth = FirebaseAuth.getInstance()

        signupBtn.setOnClickListener {
            signupValidator()
        }

        signinTextBtn.setOnClickListener {
            startActivity(Intent(this, SigninActivity::class.java))
            finish()
        }

    }

    private fun signupValidator() {

        if (fullnameSignupEdittext.text.toString().isEmpty()) {
            fullnameSignupEdittext.error = "Please enter your name."
            return
        }

        if (emailSignupEdittext.text.toString().trim().isEmpty()) {
            emailSignupEdittext.error = "Please enter your email."
            emailSigninEdittext.requestFocus()
            return
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(emailSignupEdittext.text.toString()).matches()) {
            emailSignupEdittext.error = "Please enter the valid email."
            emailSignupEdittext.requestFocus()
            return
        }

        if (passwordSignupEdittext.text.toString().trim().isEmpty()) {
            passwordSignupEdittext.error = "Please enter the password."
            passwordSignupEdittext.requestFocus()
            return
        }

        if (passwordSignupEdittext.text.toString().trim().length <8 ) {
            passwordSignupEdittext.error = "Too short."
            return
        }

        signupFirebase()

    }

    private fun signupFirebase() {
        val userResources = FirebaseDatabase.getInstance().getReference("users_resources")

        auth.createUserWithEmailAndPassword(emailSignupEdittext.text.toString(), passwordSignupEdittext.text.toString())
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    val user = auth.currentUser
                    val userUid = user!!.uid
//                    val id = userResources.push().key!!
                    val users = Users(userUid,emailSignupEdittext.text.toString(),fullnameSignupEdittext.text.toString(),"")
                    userResources.child(userUid).setValue(users)
                        .addOnSuccessListener {
                            if (task.isSuccessful) {
                                startActivity(Intent(this, SigninActivity::class.java))
                            }
                            else {
                                Toast.makeText(baseContext, "This email already exists.",
                                    Toast.LENGTH_SHORT).show()
                            }
                        }

                } else {
                    Toast.makeText(baseContext, "Authentication failed.",
                        Toast.LENGTH_SHORT).show()
                }
            }
    }


}
