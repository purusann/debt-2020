package com.example.debt.rearrangement.debtrearrangement.ui.paymentestimation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.debt.rearrangement.debtrearrangement.R

class PaymentEstimationActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_estimation)
    }
}
