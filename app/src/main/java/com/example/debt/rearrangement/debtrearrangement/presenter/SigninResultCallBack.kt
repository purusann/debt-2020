package com.example.debt.rearrangement.debtrearrangement.presenter

interface SigninResultCallBack {
    fun onAuthSuccess(message: String)
    fun onAuthError(message: String)
}