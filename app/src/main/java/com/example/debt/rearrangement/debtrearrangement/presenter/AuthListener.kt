package com.example.debt.rearrangement.debtrearrangement.presenter

import com.rengwuxian.materialedittext.MaterialEditText

interface AuthListener {
    fun onSuccess()
    fun onFailure(materialEditText: MaterialEditText, message: String)
}