package com.example.debt.rearrangement.debtrearrangement.ui.debtmanagement

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.debt.rearrangement.debtrearrangement.R
import kotlinx.android.synthetic.main.activity_debt_management.*

class DebtManagementActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_debt_management)

        newLoanBtn.setOnClickListener {
            startActivity(Intent(this, NewLoanActivity::class.java))
        }

    }

}
