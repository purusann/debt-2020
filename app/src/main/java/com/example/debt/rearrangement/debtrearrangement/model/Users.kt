package com.example.debt.rearrangement.debtrearrangement.model

data class Users(
    val uid: String,
    val email: String,
    val fullName: String,
    val password: String
)

