package com.example.debt.rearrangement.debtrearrangement.ui.auth

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns
import android.widget.Toast
import com.example.debt.rearrangement.debtrearrangement.R
import com.example.debt.rearrangement.debtrearrangement.ui.home.MainActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import kotlinx.android.synthetic.main.activity_signin.*

class SigninActivity : AppCompatActivity() {

    private lateinit var auth:FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signin)
        auth = FirebaseAuth.getInstance()

        signinBtn.setOnClickListener {
            signinValidator()
        }

        signinWithGoogleBtn.setOnClickListener {

        }

        signupTextBtn.setOnClickListener {
            startActivity(Intent(this, SignupActivity::class.java))
            finish()
        }

        forgotPasswordTextBtn.setOnClickListener {
            startActivity(Intent(this, ForgotPasswordActivity::class.java))
        }


    }

    override fun onStart() {
        super.onStart()
        val currentUser = auth.currentUser
        updateUI(currentUser)
    }

    private fun signinValidator() {

        if (emailSigninEdittext.text.toString().trim().isEmpty()) {
            emailSigninEdittext.error = "Please enter your email."
            emailSigninEdittext.requestFocus()
            return
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(emailSigninEdittext.text.toString().trim()).matches()) {
            emailSigninEdittext.error = "Please enter the valid email."
            emailSigninEdittext.requestFocus()
            return
        }

        if (passwordSigninEdittext.text.toString().trim().isEmpty()) {
            passwordSigninEdittext.error = "Please enter the password."
            passwordSigninEdittext.requestFocus()
            return
        }

        signinFirebase()

    }

    private fun signinFirebase() {
        auth.signInWithEmailAndPassword(emailSigninEdittext.text.toString().trim(), passwordSigninEdittext.text.toString().trim())
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    val user = auth.currentUser
                    updateUI(user)
                } else {
                    Toast.makeText(baseContext, "Authentication failed.",
                        Toast.LENGTH_SHORT).show()
                    updateUI(null)
                }
            }
    }

    private fun updateUI(currentUser: FirebaseUser?) {
        if (currentUser != null) {
            startActivity(Intent(this, MainActivity::class.java))
        }
    }

}
