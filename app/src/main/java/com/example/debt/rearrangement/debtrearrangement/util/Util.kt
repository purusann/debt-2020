package com.example.debt.rearrangement.debtrearrangement.util

import android.content.Context
import android.widget.Toast
import com.rengwuxian.materialedittext.MaterialEditText

fun Context.toast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_LONG).show()
}

fun Context.setError(materialEditText: MaterialEditText, message: String) {
    materialEditText.error = message
    materialEditText.requestFocus()
}