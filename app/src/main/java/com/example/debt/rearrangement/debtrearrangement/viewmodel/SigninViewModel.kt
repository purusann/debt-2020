package com.example.debt.rearrangement.debtrearrangement.viewmodel

import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.debt.rearrangement.debtrearrangement.presenter.SigninResultCallBack

class SigninViewModel (private val listener: SigninResultCallBack): ViewModel() {

    var email = ObservableField("")
    var password = ObservableField("")

    var resultData = MutableLiveData<String>()

    fun getSigninResult(): MutableLiveData<String> {
        return resultData
    }
}