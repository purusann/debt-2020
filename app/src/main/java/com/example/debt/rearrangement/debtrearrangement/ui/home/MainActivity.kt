package com.example.debt.rearrangement.debtrearrangement.ui.home

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.debt.rearrangement.debtrearrangement.R
import com.example.debt.rearrangement.debtrearrangement.ui.auth.SigninActivity
import com.example.debt.rearrangement.debtrearrangement.ui.debtmanagement.DebtManagementActivity
import com.example.debt.rearrangement.debtrearrangement.ui.loanestimation.LoanEstimationActivity
import com.example.debt.rearrangement.debtrearrangement.ui.loanratecomparison.LoanRateComparisonActivity
import com.example.debt.rearrangement.debtrearrangement.ui.paymentestimation.PaymentEstimationActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        signoutTextViewBtn.setOnClickListener {
            FirebaseAuth.getInstance().signOut()
            startActivity(Intent(this, SigninActivity::class.java))
        }

        debtManagementBtn.setOnClickListener {
            startActivity(Intent(this, DebtManagementActivity::class.java))
        }

        loanEstimationBtn.setOnClickListener {
            startActivity(Intent(this, LoanEstimationActivity::class.java))
        }

        paymentEstimationBtn.setOnClickListener {
            startActivity(Intent(this, PaymentEstimationActivity::class.java))
        }

        compareLoanRateBtn.setOnClickListener {
            startActivity(Intent(this, LoanRateComparisonActivity::class.java))
        }

    }

}
